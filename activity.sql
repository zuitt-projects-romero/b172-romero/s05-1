--1
SELECT customerName FROM customers 
WHERE country = "Philippines"

--2 
SELECT contactLastName, contactFirstName 
FROM customers WHERE customerName = "La Rochelle Gifts"

--3 
SELECT productName, MSRP 
FROM products 
WHERE productName = "The Titanic"

--4 
SELECT firstName, lastName 
FROM employees
WHERE email ="jfirrelli@classicmodelcars.com"

--5
SELECT customerName 
FROM customers
WHERE state IS NULL;

--6
SELECT firstName, lastName, email 
FROM employees
WHERE lastName = "Patterson" AND firstName = "Steve"

--7 
SELECT customerName, country, creditLimit 
FROM customers 
WHERE country != "USA" AND creditLimit > 3000;

--8
SELECT customerName 
FROM customers 
WHERE customerName NOT LIKE "%a%";

--9
SELECT orderNumber 
FROM orders 
WHERE comments LIKE "DHL";

--10
SELECT productLine 
FROM productlines 
WHERE textDescription LIKE "%state of the art";

--11
SELECT DISTINCT country 
FROM customers;

--12
SELECT DISTINCT status 
FROM orders;

--13
SELECT customerName, country 
FROM customers 
WHERE country = "USA" 
OR "France" OR "Canada"

--14 
SELECT firstName, lastName, officeCode 
FROM employees
WHERE officeCode = 5;


--15
SELECT employeeNumber 
FROM employees 
WHERE firstName = "Leslie"  
AND lastName = "Thompson"

SELECT customerName 
FROM customers 
WHERE salesRepEmployeeNumber = 1166


--16
SELECT productName, customerName 
FROM products
INNER JOIN customers
WHERE customers.customerName = "Baane Mini Imports"

--17

SELECT firstName, lastName, customerName, offices.country 
FROM customers
JOIN offices 
ON customers.country = offices.country
JOIN employees 
ON offices.officeCode = employees.officeCode;

--18
SELECT lastName, firstName 
FROM employees 
WHERE employees.reportsTo = 1143; 

--19
SELECT productName,MAX(MSRP) AS HighestPrice 
FROM products;


--20
SELECT COUNT(*) FROM customers
WHERE country = "UK"

--21

-- 21. Return the number of products per product line
SELECT productLine, COUNT(*) productCode
FROM products
GROUP BY productLine;

-- 22. Return the number of customers served by every employee
SELECT lastName, firstName, COUNT(*) customerNumber
FROM customers
JOIN employees ON salesRepEmployeeNumber = employeeNumber
GROUP BY salesRepEmployeeNumber;


--23
SELECT productName, quantityInStock 
FROM products
WHERE productLine = "Planes"
 AND quantityInStock < 1000;